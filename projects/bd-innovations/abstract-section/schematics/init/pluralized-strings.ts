import {classify, dasherize} from '@angular-devkit/core/src/utils/strings';
import * as pluralize from 'pluralize';

export function pluralizedDasherize(value: string): string {
  const splitted: string[] = dasherize(value).split('-');
  const lastWord = splitted[splitted.length - 1];

  splitted[splitted.length - 1] = pluralize(lastWord, 2);

  return splitted.join('-');
}

export function pluralizedClassify(value: string): string {
  const splitted: string[] = classify(value).split(/(?=[A-Z])/);
  const lastWord = splitted[splitted.length - 1];

  splitted[splitted.length - 1] = pluralize(lastWord, 2);

  return splitted.join('');
}
