import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormGroup, ReactiveFormsModule} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

import {<%= classify(name) %>EditComponent} from './<%= dasherize(name) %>-edit.component';
import {<%= pluralizedClassify(name) %>Service} from '../<%= pluralizedDasherize(name) %>.service';
import {<%= classify(name) %>Logic} from '../<%= dasherize(name) %>.logic';

@Component({
  selector: 'app-bd-edit-dialog',
  template: '<ng-content></ng-content>',
  styles: []
})
export class BdEditDialogMockComponent {
  @Input() modelName: string;
  @Input() disableSubmit: boolean;
  @Input() customContent: boolean;
  @Input() form: FormGroup;
  @Input() keyValue = 'uuId';

  @Output() submit: EventEmitter<any> = new EventEmitter();
  @Output() cancel: EventEmitter<any> = new EventEmitter();
}

describe('<%= classify(name) %>EditComponent', () => {
  let component: <%= classify(name) %>EditComponent;
  let fixture: ComponentFixture<<%= classify(name) %>EditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule
      ],
      declarations: [
        <%= classify(name) %>EditComponent,

        BdEditDialogMockComponent
      ],
      providers: [
        {provide: MAT_DIALOG_DATA, useValue: {}},
        {provide: MatDialogRef, useValue: {}},
        {provide: <%= pluralizedClassify(name) %>Service, useValue: {}},
        {provide: <%= classify(name) %>Logic, useValue: {}}
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(<%= classify(name) %>EditComponent);
    component = fixture.componentInstance;
  }));

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

});
