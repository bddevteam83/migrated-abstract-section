import {Inject, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {AbstractDialogComponent} from '../abstract-dialog.component';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AbstractDialogConfig} from '../../config/abstract-dialog.config';
import {AbstractService} from '../../providers/abstract.service';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';


/**
 * @deprecated
 * */
export abstract class AbstractEditComponent<T> extends AbstractDialogComponent implements OnInit {
  /**
   * by default true, override with false when data already exist
   */
  requestInitialData = true;
  form: FormGroup;
  /**
   * is truly while http requests are pending
   */
  pending = false;

  protected constructor(@Inject(MAT_DIALOG_DATA) public data: AbstractDialogConfig<T>,
                        public service: AbstractService<T>,
                        public dialogRef: MatDialogRef<AbstractEditComponent<T>, T>) {
    super(data, dialogRef);
  }

  /**
   * if element has a keyValue & requestInitialData is true  sets pending as true
   * & calls getInitData() & subscribes on it
   *  - on response calls onSuccessfulDataInit()
   *  - on error calls onDataInitError()
   * else calls onNullElementOrKeyValue()
   */
  ngOnInit() {
    if (this.data.element && this.data.element[this.service.keyValue] && this.requestInitialData) {
      this.pending = true;
      this.getInitData()
        .subscribe(
          res => this.onSuccessfulDataInit(res),
          error => this.onDataInitError(error)
        );
    } else {
      this.onNullElementOrKeyValue();
    }
  }

  /**
   *   returns service's getOneByKeyValue(keyValue)
   */
  getInitData(): Observable<T> {
    return this.service.getOneByKeyValue(this.data.element[this.service.keyValue]);
  }

  /**
   * puts API response into data.element
   * creates form with API response value
   * sets pending as false
   * calls afterDataInit()
   * @param res: API response
   */
  onSuccessfulDataInit(res: T): void {
    this.data.element = res;
    this.form = this.returnForm(res);
    this.pending = false;
    this.afterDataInit();
  }

  /**
   * closes dialog
   * @param error: API error
   */
  onDataInitError(error): void {
    this.close();
  }

  /**
   * creates form with data.element
   * calls afterDataInit()
   */
  onNullElementOrKeyValue(): void {
    this.form = this.returnForm(this.data.element);
    this.afterDataInit();
  }

  /**
   * use for any custom logic after getting data from API
   */
  afterDataInit(): void {
  }

  abstract returnForm(obj?: T): FormGroup;

  /**
   * sets pending as true
   * check keyValue of element, if it's true will call putRequest() else will call postRequest()
   * @param submit: optional - will call API with provided submit value or take form value
   */
  submit(val?: any) {
    const isExisting = !!this.data.element[this.service.keyValue];

    this.pending = true;
    (isExisting
        ? this.putRequest(val || this.form.value)
        : this.postRequest(val || this.form.value)
    ).pipe(
      tap(
        () => (isExisting
            ? this.service.notifier && this.service.notifier.openPutNotification(this.service.config.modelName)
            : this.service.notifier && this.service.notifier.openPostNotification(this.service.config.modelName)
        ),
        (e: any) => this.service.notifier && this.service.notifier.openErrorNotification(e)
      )
    ).subscribe(
      res => this.close(res),
      err => this.onRequestError(err)
    );
  }

  /**
   * returns service's post method
   * @param submit: any value to send into post
   */
  postRequest(submit: T): Observable<T> {
    return this.service.post(submit);
  }

  /**
   * returns service's put method
   * @param submit: any value to send into put
   */
  putRequest(submit: T): Observable<T> {
    return this.service.put(submit);
  }

  /**
   * handle request error if needed
   * sets pending as false
   * @param err: API response error
   */
  onRequestError(err) {
    this.pending = false;
  }

}


