// import {AbstractEditComponent} from './abstract-edit.component';
// import {Component, Inject} from '@angular/core';
// import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
// import {AbstractDialogConfig} from '../../config/abstract-dialog.config';
// import {async, ComponentFixture, TestBed} from '@angular/core/testing';
// import {FormBuilder, FormGroup} from '@angular/forms';
// import {AbstractService} from '../../providers/abstract.service';
// import {of} from 'rxjs';
//
// describe('AbstractEditComponent', () => {
//   let component: AbstractEditComponent<T>;
//   let fixture: ComponentFixture<AbstractEditComponent<T>>;
//
//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       declarations: [EditComponent],
//       providers: [
//         {provide: MAT_DIALOG_DATA, useValue: {
//           element: t
//           }},
//         {
//           provide: MatDialogRef, useValue: {
//             close: jasmine.createSpy('close')
//           }
//         },
//         {
//           provide: AbstractService, useValue: {
//           get: jasmine.createSpy('get').and.returnValue(of(t))
//           }}
//       ]
//     })
//       .compileComponents();
//   }));
//
//   beforeEach(() => {
//     fixture = TestBed.createComponent(EditComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });
//
//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
//
//   it('should create form', () => {
//     expect(component.form).toBeTruthy();
//   });
//
//   it('should close dialog with form value on submit', () => {
//     component.form.patchValue(t);
//     component.submit();
//     expect(TestBed.get(MatDialogRef).close).toHaveBeenCalledWith(t);
//   });
//
//
// });
//
// class T {
//   id: {};
//   uuId: string;
// }
//
// const t: T = {uuId: '0123', id: {timestamp: 444444}};
//
// @Component({selector: 'bd-test-edit', template: ''})
// class EditComponent extends AbstractEditComponent<T> {
//   constructor(@Inject(MAT_DIALOG_DATA) public data: AbstractDialogConfig<T>,
//               public service: AbstractService<T>,
//               public dialogRef: MatDialogRef<AbstractEditComponent<T>>) {
//     super(data, service, dialogRef);
//   }
//
//   returnForm(obj?: T): FormGroup {
//     return new FormBuilder().group({uuId: '', id: ''});
//   }
//   afterDataInit(): void {
//   }
// }
