import {Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

/**
 * @deprecated
 * */
export abstract class AbstractDialogComponent {

  protected constructor(@Inject(MAT_DIALOG_DATA) public data: any,
                        public dialogRef: MatDialogRef<AbstractDialogComponent>) {
  }

  close = (val?: any) => this.dialogRef.close(val || null);

  abstract submit(val?: any);

}
