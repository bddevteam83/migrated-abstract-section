// import {async, ComponentFixture, TestBed} from '@angular/core/testing';
//
// import {AbstractDeleteComponent} from './abstract-delete.component';
// import {MAT_DIALOG_DATA, MatCardModule, MatDialogModule, MatDialogRef} from '@angular/material';
// import {Component, Inject, Pipe, PipeTransform} from '@angular/core';
// import {AbstractDialogConfig} from '../../config/abstract-dialog.config';
//
// describe('AbstractDeleteComponent', () => {
//   let component: DeleteComponent;
//   let fixture: ComponentFixture<AbstractDeleteComponent<T>>;
//
//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       declarations: [DeleteComponent, TranslateMock],
//       imports: [MatCardModule, MatDialogModule],
//       providers: [
//         {provide: MAT_DIALOG_DATA, useValue: {}},
//         {
//           provide: MatDialogRef, useValue: {
//             'close': jasmine.createSpy('close')
//           }
//         },
//       ]
//     })
//       .compileComponents();
//   }));
//
//   beforeEach(() => {
//     fixture = TestBed.createComponent(DeleteComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });
//
//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
//
//   it('should close with "true" on submit', () => {
//     component.submit();
//     expect(TestBed.get(MatDialogRef).close).toHaveBeenCalledWith(true);
//   });
// });
//
// class T {
//   id: {};
//   uuId: string;
// }
//
// @Component({selector: 'bd-test-delete', template: ''})
// class DeleteComponent extends AbstractDeleteComponent<T> {
//   constructor(@Inject(MAT_DIALOG_DATA) public data: AbstractDialogConfig<T>,
//               public dialogRef: MatDialogRef<AbstractDeleteComponent<T>, boolean>) {
//     super(data, dialogRef);
//   }
//
//   returnDeleteMessage(obj: T): string {
//     return 'message';
//   }
//
//   returnDeleteTitle(): string {
//     return 'title';
//   }
//
// }
//
// @Pipe({name: 'translate'})
// class TranslateMock implements PipeTransform {
//   transform(value: any, ...args: any[]): any {
//     return value;
//   }
// }
