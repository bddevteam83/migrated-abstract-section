import {Inject} from '@angular/core';
import {AbstractDialogComponent} from '../abstract-dialog.component';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AbstractDialogConfig} from '../../config/abstract-dialog.config';
import {AbstractService} from '../../providers/abstract.service';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

/**
 * @deprecated
 * */
export abstract class AbstractDeleteComponent<T> extends AbstractDialogComponent {

  /**
   * is truly while http requests are pending
   */
  pending = false;

  protected constructor(@Inject(MAT_DIALOG_DATA) public data: AbstractDialogConfig<T>,
                        public service: AbstractService<T>,
                        public dialogRef: MatDialogRef<AbstractDeleteComponent<T>, boolean>) {
    super(data, dialogRef);
  }

  /**
   * sets pending as true;
   * calls deleteRequest() & subscribes on result
   * when ok: calls onSuccessfulDelete(response),
   * on error: calls onDeleteFail(error)
   */
  submit() {
    this.pending = true;
    this.deleteRequest().pipe(
      tap(
        () => this.service.notifier && this.service.notifier.openDeleteNotification(this.service.config.modelName),
        (e: any) => this.service.notifier && this.service.notifier.openErrorNotification(e)
      )
    ).subscribe(
      res => this.onSuccessfulDelete(res),
      err => this.onDeleteFail(err)
    );
  }

  /**
   * calls service's delete()
   */
  deleteRequest(): Observable<any> {
    return this.service.delete(this.data.element[this.service.keyValue]);
  }

  /**
   * closes dialog with http response
   * @param res http response
   */
  onSuccessfulDelete(res) {
    this.pending = false;
    this.close(res);
  }

  /**
   * sets pending as false
   * @param err - http error
   */
  onDeleteFail(err: any) {
    this.pending = false;
  }

}
