// import {async, ComponentFixture, TestBed} from '@angular/core/testing';
//
// import {AbstractDialogComponent} from './abstract-dialog.component';
// import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
// import {Component, Inject, Pipe, PipeTransform} from '@angular/core';
// import {AbstractDialogConfig} from '../config/abstract-dialog.config';
//
// describe('AbstractDialogComponent', () => {
//   let component: AbstractDialogComponent<T>;
//   let fixture: ComponentFixture<AbstractDialogComponent<T>>;
//   let dialogRef: MatDialogRef<T>;
//
//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       declarations: [DialogComponentFasade, TranslateMock],
//       providers: [
//         {provide: MAT_DIALOG_DATA, useValue: {}},
//         {
//           provide: MatDialogRef, useValue: {
//             'close': jasmine.createSpy('close')
//           }
//         },
//       ]
//     })
//       .compileComponents();
//   }));
//
//   beforeEach(() => {
//     fixture = TestBed.createComponent(DialogComponentFasade);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//     dialogRef = TestBed.get(MatDialogRef);
//   });
//
//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
//
//   it('onClose should close dialog without args', () => {
//     component.close();
//     expect(dialogRef.close).toHaveBeenCalledWith(null);
//   });
//
//   it('onSubmit should close dialog with submit args', () => {
//     const t = {uuId: '', id: {}};
//     component.submit(t);
//     expect(dialogRef.close).toHaveBeenCalledWith(t);
//   });
// });
//
// class T implements AbstractModel {
//   id: {};
//   uuId: string;
// }
//
// @Pipe({name: 'translate'})
// class TranslateMock implements PipeTransform {
//   transform(value: any, ...args: any[]): any {
//     return value;
//   }
// }
//
// @Component({selector: 'bd-test-dialog', template: ''})
// class DialogComponentFasade extends AbstractDialogComponent<T> {
//   constructor(@Inject(MAT_DIALOG_DATA) public data: AbstractDialogConfig<T>,
//               public dialogRef: MatDialogRef<AbstractDialogComponent<T>>) {
//     super(data, dialogRef);
//   }
// }
