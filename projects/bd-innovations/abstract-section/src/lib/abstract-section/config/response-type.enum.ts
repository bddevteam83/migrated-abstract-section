/**
 * @deprecated
 * */
export enum ResponseTypeEnum {
  all = 'all',
  paginated = 'paginated',
  /**
   * @deprecated
   */
  paginated_post = 'post',
  /**
   * @deprecated
   */
  paginated_path = 'path'
}
