/**
 * @deprecated
 * */
export interface AbstractServiceConfig {
  apiPrefix: string;
  apiPath: {
    getOne: string;
    getAll: string;
    getPaginated: string;
    post: string;
    put: string;
    delete: string;
  };

  /**
   * @deprecated is not used since we created AbstractNotifierService with its logic
   */
  successMessagePath?: {
    get: string;
    post: string;
    put: string;
    delete: string;
  };

  modelName: string;
}
