/**
 * @deprecated
 * */
export interface PaginatedResponseModel<T> {
  numberOfEntities?: number;
  totalItems?: number;
  list: T[];
}
