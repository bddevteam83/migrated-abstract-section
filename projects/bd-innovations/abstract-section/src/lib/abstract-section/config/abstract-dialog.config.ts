/**
 * @deprecated
 * */
export interface AbstractDialogConfig<T> {
  element?: T;
}

