import {ApiRequestModel} from './dynamic-table-models/api-request.model';

/**
 * @deprecated
 * */
export interface ApiCallModel extends ApiRequestModel {
  filters?: {};
}
