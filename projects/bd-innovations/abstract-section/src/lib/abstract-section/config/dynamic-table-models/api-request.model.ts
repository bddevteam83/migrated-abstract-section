/**
 * @deprecated
 * */
export interface ApiRequestModel {
  search: IndexedSearchRequestModel;
  paginator: PaginationRequestModel;
  sort: SortRequestModel;
}

export type IndexedSearchRequestModel = string;

export interface PaginationRequestModel {
  pageIndex: number;
  pageSize: number;
  length?: number;
}

export interface SortRequestModel {
  active: string;
  direction: UpperCaseSortDirection;
}

export type UpperCaseSortDirection = 'ASC' | 'DESC' | null;

