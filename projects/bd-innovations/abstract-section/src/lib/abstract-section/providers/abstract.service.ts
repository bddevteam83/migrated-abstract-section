import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {TranslateService} from '@ngx-translate/core';
import {AbstractServiceConfig} from '../config/abstract-service.config';
import {ApiCallModel} from '../config/api-call.model';
import {PaginatedResponseModel} from '../config/paginated-response.model';
import {AbstractNotifierService} from './abstract-notifier.service';
import {Optional} from '@angular/core';

/**
 * @deprecated
 * */
export class AbstractService<T> {
  /**
   * by default keyValue is uuId, override if need other
   */
  keyValue = 'uuId';

  protected constructor(protected http: HttpClient,
                        protected translate: TranslateService,
                        public config: AbstractServiceConfig,
                        @Optional() public notifier?: AbstractNotifierService) {
  }

  /**
   * @param key - uuId or ObjectId or...
   */
  getOneByKeyValue(key: any): Observable<T> {
    const path = `${this.config.apiPrefix}${this.config.apiPath.getOne}${key}`;

    return this.http.get<T>(path);
  }

  /**
   * @param parentKey - provide when request is send for child entity
   */
  getAll(parentKey?: string | number): Observable<T[]> {
    const path = `${this.config.apiPrefix}${this.config.apiPath.getAll}${parentKey ? '/' + parentKey : ''}`;

    return this.http.get<T[]>(path);
  }

  /**
   *
   * @param apiCall paginator & search settings
   * @param parentKey - provide when request is send for child entity
   */
  getAllPaginated(apiCall: ApiCallModel, parentKey?: string | number): Observable<PaginatedResponseModel<T>> {
    const {apiPrefix, apiPath} = this.config;
    const {paginator: {pageIndex, pageSize}, search, sort, filters} = apiCall;
    const path = `${apiPrefix}${apiPath.getPaginated}/${pageIndex}/${pageSize}${parentKey ? '/' + parentKey : ''}`;
    const params: any = {...filters};
    search && (params.search = search);
    sort && sort.direction && sort.active && (params.sort = `${sort.active},${sort.direction}`);

    const stringifiedParams: { [key: string]: string } = {};
    Object.keys(params).forEach(key => {
      if (params[key] instanceof Date) {
        stringifiedParams[key] = (params[key]).toISOString();
      } else if (params[key] !== '' && params[key] !== null && params[key] !== undefined) {
        stringifiedParams[key] = params[key];
      }
    });


    return this.http.get<PaginatedResponseModel<T>>(path, {params: stringifiedParams});
  }

  /**
   * @deprecated use getAllPaginated instead
   * @param apiCall paginator & search settings
   * @param parentKey - provide when request is send for child entity
   */
  getAllPaginatedBody(apiCall: ApiCallModel, parentKey?: string | number): Observable<PaginatedResponseModel<T>> {
    const {apiPrefix, apiPath} = this.config;
    const path = `${apiPrefix}${apiPath.getPaginated}${parentKey ? '/' + parentKey : ''}`;
    const body = Object.keys(apiCall).reduce((res, key) =>
      key === 'filters' ? {...res, ...apiCall.filters} : {...res, [key]: apiCall[key]}, {});

    return this.http.post<PaginatedResponseModel<T>>(path, body);
  }

  /**
   * @deprecated use getAllPaginated instead
   * @param apiCall paginator & search settings
   * @param parentKey - provide when request is send for child entity
   */
  getAllPaginatedPath(apiCall: ApiCallModel, parentKey?: string | number): Observable<PaginatedResponseModel<T>> {
    const {apiPrefix, apiPath} = this.config;
    const {paginator: {pageIndex, pageSize}, search} = apiCall;
    const path = `${apiPrefix}${apiPath.getPaginated}${parentKey ? '/' + parentKey : ''}/${pageIndex}/${pageSize}/${search}`;
    const body = Object.keys(apiCall).reduce((res, key) =>
      key === 'filters' ? {...res, ...apiCall.filters} : {...res, [key]: apiCall[key]}, {});
    delete body['paginator'];

    return this.http.post<PaginatedResponseModel<T>>(path, body);
  }

  /**
   * @param newObject: create new element
   */
  post(newObject: T): Observable<T> {
    const {apiPrefix, apiPath} = this.config;
    const path = `${apiPrefix}${apiPath.post}`;
    return this.http.post<T>(path, newObject);
  }

  /**
   * @param editedObject: edit element
   */
  put(editedObject: T): Observable<T> {
    const {apiPrefix, apiPath} = this.config;
    const path = `${apiPrefix}${apiPath.put}`;
    return this.http.put<T>(path, editedObject);
  }

  /**
   * @param key: delete element by keyValue
   */
  delete(key: string): Observable<T> {
    const {apiPrefix, apiPath} = this.config;
    const path = `${apiPrefix}${apiPath.delete}${key}`;
    return this.http.delete<T>(path);
  }

}
