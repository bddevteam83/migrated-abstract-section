/**
 * @deprecated
 * */
export abstract class AbstractNotifierService {

  protected constructor() {
  }

  abstract openGetOneNotification(modelNamePrefix?: string): void;

  abstract openGetListNotification(modelNamePrefix?: string, amount?: number, totalAmount?: number): void;

  abstract openPostNotification(modelNamePrefix?: string): void;

  abstract openPutNotification(modelNamePrefix?: string): void;

  abstract openDeleteNotification(modelNamePrefix?: string): void;

  abstract openErrorNotification(error?: any): void;

}
