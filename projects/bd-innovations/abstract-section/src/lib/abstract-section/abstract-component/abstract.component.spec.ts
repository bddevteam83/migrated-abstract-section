// import {async, ComponentFixture, TestBed} from '@angular/core/testing';
// import {AbstractComponent} from './abstract.component';
// import {AbstractService} from '../providers/abstract.service';
// import {Component} from '@angular/core';
// import {MatDialog} from '@angular/material';
// import {ComponentType} from '@angular/cdk/portal';
// import {AbstractDeleteComponent} from '../abstract-dialog/abstract-delete/abstract-delete.component';
// import {AbstractEditComponent} from '../abstract-dialog/abstract-edit/abstract-edit.component';
//
// describe('AbstractComponent', () => {
//   let component: AbstractComponent<T>;
//   let fixture: ComponentFixture<AbstractComponent<T>>;
//
//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       declarations: [SomeComponent],
//       providers: [
//         {provide: AbstractService, useValue: {}},
//         {provide: MatDialog, useValue: {}}
//       ]
//     })
//       .compileComponents();
//   }));
//
//   beforeEach(() => {
//     fixture = TestBed.createComponent(SomeComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });
//
//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
// });
//
// class T {
//   id: {};
//   uuId: string;
// }
//
// const t: T = {uuId: '0123', id: {timestamp: 444444}};
//
// @Component({selector: 'bd-test-edit', template: ''})
// class SomeComponent extends AbstractComponent<T> {
//   constructor(public service: AbstractService<T>,
//               public dialog: MatDialog) {
//     super(service, dialog);
//   }
//
//   openDelete(obj?: T): void {
//   }
//
//   openEdit(obj?: T): void {
//   }
//
//   openNew(): void {
//   }
//
//   returnDeleteComponentType(): ComponentType<AbstractDeleteComponent<T>> {
//     return undefined;
//   }
//
//   returnEditComponentType(): ComponentType<AbstractEditComponent<T>> {
//     return undefined;
//   }
//
//   onSuccessfulCreateNew(res: any): void {
//   }
//
//   onSuccessfulDelete(res: any): void {
//   }
//
//   onSuccessfulEdit(res: any): void {
//   }
// }
