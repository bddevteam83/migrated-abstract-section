import {OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {AbstractComponent} from '../abstract.component';
import {AbstractService} from '../../providers/abstract.service';
import {Observable} from 'rxjs';
import {filter, map, switchMap, takeUntil, tap} from 'rxjs/operators';

/**
 * @deprecated
 * */
export abstract class AbstractDetailsComponent<T> extends AbstractComponent<T> implements OnInit {
  element: T;
  previousUrl: string;
  /**
   * override if param doesn't match with keyValue in service
   */
  pathParamName: string;

  get _pathParamName(): string {
    return this.pathParamName || this.service.keyValue;
  }

  protected constructor(public service: AbstractService<T>,
                        public dialog: MatDialog,
                        public router: Router,
                        public route: ActivatedRoute) {
    super(service, dialog);
  }

  /**
   * calls setPreviousUrl(), setPathParamName()
   * gets keyValue from route & emits it into getData
   */
  ngOnInit() {
    this.setPreviousUrl();
    super.ngOnInit();
  }

  setPreviousUrl(): void {
    this.router.events.pipe(
      takeUntil(this.onDestroy$),
      filter(event => event instanceof NavigationEnd)
    ).subscribe((event: NavigationEnd) => {
      this.previousUrl = event.url;
    });
  }

  /**
   * calls getRequest(key) if requestInitialData != false
   * on response calls onSuccessfulDataInit()
   * on error - onDataInitError()
   * @param key: keyValue of element
   */
  getData(key: string) {
    this.route.params.pipe(
      takeUntil(this.onDestroy$),
      filter(params => params && params[this._pathParamName]),
      map(params => params[this._pathParamName]),
      switchMap(paramKey => this.getRequest(paramKey)),
      tap(
        () => this.service.notifier && this.service.notifier.openGetOneNotification(this.service.config.modelName),
        (e: any) => this.service.notifier && this.service.notifier.openErrorNotification(e)
      )
    ).subscribe(
      res => this.onSuccessfulDataInit(res),
      error => this.onDataInitError(error)
    );
  }

  getRequest(key: any): Observable<T> {
    return this.service.getOneByKeyValue(key);
  }

  /**
   * puts API response into element;
   * then calls afterDataInit()
   * @param res - API response
   */
  onSuccessfulDataInit(res: T): void {
    this.element = res;
    this.afterDataInit();
  }

  openEdit(t?: T): void {
    super.openEdit(t || this.element);
  }

  onSuccessfulEdit(res: T): void {
    this.element = res;
  }

  openDelete(t?: T): void {
    super.openDelete(t || this.element);
  }

  /**
   * calls back()
   * @param res: deleted element
   */
  onSuccessfulDelete(res: T): void {
    this.back();
  }

  back(): void {
    this.router.navigate([this.previousUrl]);
  }

}
