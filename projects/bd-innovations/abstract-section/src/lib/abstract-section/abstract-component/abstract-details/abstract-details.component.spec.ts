// import {async, ComponentFixture, TestBed} from '@angular/core/testing';
//
// import {AbstractDetailsComponent} from './abstract-details.component';
// import {ActivatedRoute, Router} from '@angular/router';
// import {of} from 'rxjs';
// import {Component} from '@angular/core';
// import {ComponentType} from '@angular/cdk/portal';
// import {AbstractEditComponent} from '../../abstract-dialog/abstract-edit/abstract-edit.component';
// import {AbstractDeleteComponent} from '../../abstract-dialog/abstract-delete/abstract-delete.component';
// import {MatDialog} from '@angular/material';
// import {AbstractService} from '../../providers/abstract.service';
//
// describe('AbstractDetailsComponent', () => {
//   let component: DetailsComponent;
//   let fixture: ComponentFixture<DetailsComponent>;
//   const t: T = {uuId: '0123', id: {}};
//   const r: T = {uuId: '0123', id: {}, name: '123'};
//
//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       declarations: [DetailsComponent],
//       providers: [
//         {
//           provide: AbstractService, useValue: {
//             get: jasmine.createSpy('get').and.returnValue(of(t)),
//             put: jasmine.createSpy('put').and.returnValue(of(r)),
//             delete: jasmine.createSpy('delete').and.returnValue(of(r)),
//           }
//         },
//         {
//           provide: ActivatedRoute, useValue: {
//             params: of(t.uuId)
//           }
//         },
//         {
//           provide: MatDialog, useValue: {
//             open: jasmine.createSpy('open')
//           }
//         },
//         {provide: Router, useValue: {events: of({})}}
//       ]
//     })
//       .compileComponents();
//   }));
//
//   beforeEach(() => {
//     fixture = TestBed.createComponent(DetailsComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });
//
//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
//
//   it('should receive element', () => {
//     expect(component.element).toEqual(t);
//   });
//
//   // it('should change element after edit', () => {
//   //   component.openEdit();
//   //   // TestBed.get(MatDialog)
//   //   // component.
//   //   expect(component.element).toEqual(r);
//   // });
// });
//
// class T {
//   id: {};
//   uuId: string;
//   name?: string;
// }
//
// @Component({selector: 'bd-test-details', template: ''})
// class DetailsComponent extends AbstractDetailsComponent<T> {
//
//   public constructor(public service: AbstractService<T>,
//                      public dialog: MatDialog,
//                      public router: Router,
//                      public route: ActivatedRoute) {
//     super(service, dialog, router, route);
//   }
//
//   returnDeleteComponentType(): ComponentType<AbstractDeleteComponent<T>> {
//     return undefined;
//   }
//
//   returnEditComponentType(): ComponentType<AbstractEditComponent<T>> {
//     return undefined;
//   }
//
// }
