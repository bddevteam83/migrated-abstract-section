import {Directive, Input, OnDestroy, OnInit} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {AbstractDialogConfig} from '../config/abstract-dialog.config';
import {AbstractService} from '../providers/abstract.service';
import {ComponentType} from '@angular/cdk/portal';
import {AbstractEditComponent} from '../abstract-dialog/abstract-edit/abstract-edit.component';
import {AbstractDialogComponent} from '../abstract-dialog/abstract-dialog.component';
import {Subject} from 'rxjs';

/**
 * @deprecated
 * */
@Directive()
export abstract class AbstractComponent<T> implements OnInit, OnDestroy {

  dialog_config: MatDialogConfig = {};
  abstract_dialog_config: AbstractDialogConfig<T> = {};

  /**
   * by default true, override with false when data already exist
   */
  @Input() requestInitialData = true;

  /** DIALOGS SETTINGS */
  dialogPanelClass = 'bd-dialog';
  editDialogWidth = '700px';
  editDialogPanelClass = 'bd-edit-dialog';
  newDialogWidth: string = undefined;
  newDialogPanelClass: string = undefined;
  deleteDialogWidth = '500px';
  deleteDialogPanelClass = 'bd-delete-dialog';

  readonly onDestroy$: Subject<void> = new Subject<void>();

  protected constructor(public service: AbstractService<T>,
                        public dialog: MatDialog) {
  }


  /**
   * calls getData() if requestInitData is true (by default it is)
   */
  ngOnInit(): void {
    if (this.requestInitialData) {
      this.getData();
    }
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  abstract getData(key?);

  onSuccessfulDataInit(res: any): void {
  }

  afterDataInit(): void {
  }

  onDataInitError(error): void {
  }

  abstract returnEditComponentType(): ComponentType<AbstractEditComponent<T>>;

  abstract returnDeleteComponentType(): ComponentType<AbstractDialogComponent>;

  /** EDIT DIALOG */
  /**
   * calls onBeforeEditOpen()
   * opens dialog
   * subscribe on dialog afterClosed and emits value into afterEditClosed()
   * @param t: element
   */
  openEdit(t: T) {
    this.onBeforeEditOpen(t);
    const dialogRef = this.dialog.open(this.returnEditComponentType(), this.dialog_config);
    dialogRef.afterClosed().subscribe(res => this.afterEditClose(res));
  }

  /**
   * setup dialog data with t, dialog width with editDialogWidth & dialog panelClasses with dialogPanelClass & editDialogPanelClass
   * @param t: element
   */
  onBeforeEditOpen(t: T) {
    this.abstract_dialog_config.element = t;
    this.dialog_config.data = this.abstract_dialog_config;
    this.dialog_config.width = this.editDialogWidth;
    this.dialog_config.panelClass = [this.dialogPanelClass, this.editDialogPanelClass];
  }

  /**
   * if submit true calls onSuccessfulEdit()
   * when false onEditCancel()
   * @param submit: true when edited/false when canceled
   */
  afterEditClose(submit: boolean | any): void {
    submit ? this.onSuccessfulEdit(submit) : this.onEditCancel();
  }

  onSuccessfulEdit(submit: any): void {
    this.getData();
  }

  onEditCancel(): void {
  }

  /** DELETE DIALOG */
  /**
   * calls onBeforeDeleteOpen()
   * opens dialog
   * subscribe on dialog afterClosed and emits value into afterDeleteClose()
   * @param t: element
   */
  openDelete(t: T) {
    this.onBeforeDeleteOpen(t);
    const dialogRef = this.dialog.open(this.returnDeleteComponentType(), this.dialog_config);
    dialogRef.afterClosed().subscribe(submit => this.afterDeleteClose(submit, t));
  }

  /**
   * setup dialog data with t, dialog width with deleteDialogWidth & dialog panelClasses with dialogPanelClass & deleteDialogPanelClass
   * @param t: element
   */
  onBeforeDeleteOpen(t: T): void {
    this.abstract_dialog_config.element = t;
    this.dialog_config.data = this.abstract_dialog_config;
    this.dialog_config.width = this.deleteDialogWidth;
    this.dialog_config.panelClass = [this.dialogPanelClass, this.deleteDialogPanelClass];
  }

  /**
   * if submit true calls onSuccessfulDelete()
   * when false onDeleteCancel()
   * @param submit: true when edited/false when canceled
   * @param t: element to delete
   */
  afterDeleteClose(submit: boolean, t: T | any): void {
    submit ? this.onSuccessfulDelete(submit, t) : this.onDeleteCancel();
  }

  /**
   * calls getData()
   * @param submit: true when edited/false when canceled
   * @param t: element to delete
   */
  abstract onSuccessfulDelete(submit, t): void;

  onDeleteCancel(): void {
  }

}
