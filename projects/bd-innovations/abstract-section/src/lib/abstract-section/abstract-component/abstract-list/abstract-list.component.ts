import {Directive, Input, OnDestroy} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {AbstractComponent} from '../abstract.component';
import {AbstractService} from '../../providers/abstract.service';
import {ResponseTypeEnum} from '../../config/response-type.enum';
import {Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {PaginatedResponseModel} from '../../config/paginated-response.model';
import {ApiCallModel} from '../../config/api-call.model';
import {PaginationRequestModel} from '../../config/dynamic-table-models/api-request.model';
import {ComponentType} from '@angular/cdk/overlay';
import {AbstractEditComponent} from '../../abstract-dialog/abstract-edit/abstract-edit.component';

/**
 * @deprecated
 * */
@Directive()
export abstract class AbstractListComponent<T> extends AbstractComponent<T> implements OnDestroy {
  /**
   * data: by default empty array, if Inputted from other component override ngOnInit, to disable get All request
   */
  @Input() data: T[] = [];
  /**
   * parentUuId: if provided will call getAll API with request like GET 'model/{parentUuId}'
   * provide for child entities
   */
  @Input() parentUuId: string;

  /**
   * override for child entities used for list & details
   * @example
   * ```
   * get newElement() {
   *     return {someoneUuId: this.parentUuId}
   * }
   * ```
   */
  get newElement() {
    return {};
  }

  /**
   * listType: By default gets all, if need paginated override with ResponseTypeEnum.paginated/paginated_body/paginated_path
   */
  listType: ResponseTypeEnum = ResponseTypeEnum.all;
  /**
   * filters - object {key: value} that will be mapped into request
   * for paginated: &key=value
   */
  filters: {
    [key: string]: any;
  } = {};

  /**
   * TABLE SETTINGS
   * sort - true by default, if sort is available
   * apiable - true by default, if search & pages change will call API
   * search - true by default, if search is available in table
   * paginator - by default pageSize = 25, pageIndex = 0, length = null
   */
  sort = true;
  apiable = true;
  search = true;
  paginator: PaginationRequestModel = {
    pageSize: 25,
    pageIndex: 0,
    length: null
  };

  protected constructor(public service: AbstractService<T>,
                        public dialog: MatDialog,
                        public router: Router,
                        public route: ActivatedRoute) {
    super(service, dialog);
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.data = undefined;
  }

  returnNewComponentType(): ComponentType<AbstractEditComponent<T>> {
    return undefined;
  }

  /**
   * combines this.paginator, this filters & apiCallModel from arguments
   * @param obj - optional ApiCallModel
   */
  getApiCallModel(obj?: ApiCallModel | any): ApiCallModel {
    return {
      paginator: this.paginator,
      filters: this.filters,
      ...obj
    };
  }

  /**
   * use for dynamic table @Output(apiRequest)
   * @param apiCall: ApiCallModel
   */
  onApiRequestEmitted(apiCall: ApiCallModel | any) {
    this.getData({...apiCall} as ApiCallModel);
  }

  /**
   * calls getAllRequest() maps it with mapListResponse() and subscribes on result.
   * If request successful calls onSuccessfulDataInit(res),
   * on error calls onDataInitError(error)
   * @param apiCall: optional ApiCallModel
   */
  getData(apiCall?: ApiCallModel | any): void {
    this.getAllRequest(this.getApiCallModel(apiCall))
      .subscribe(
        res => this.onSuccessfulDataInit(res),
        error => this.onDataInitError(error)
      );
  }

  /**
   * depending on listType calls service's getAll() or one of getAllPaginated()
   * @param apiCall: ApiCallModel better format throw getApiCallModel(apiCall) before
   */
  getAllRequest(apiCall: ApiCallModel): Observable<T[]> {
    switch (this.listType) {
      case ResponseTypeEnum.paginated:
        return this.service.getAllPaginated(apiCall, this.parentUuId).pipe(
          tap(
            ({list, totalItems}) => this.service.notifier && this.service.notifier.openGetListNotification(this.service.config.modelName, list.length, totalItems),
            (e: any) => this.service.notifier && this.service.notifier.openErrorNotification(e)
          ),
          map(res => this.mapPaginatedResponse(res, apiCall)));
      case ResponseTypeEnum.paginated_path:
        return this.service.getAllPaginatedPath(apiCall, this.parentUuId).pipe(
          tap(
            ({list, numberOfEntities}) => this.service.notifier && this.service.notifier.openGetListNotification(this.service.config.modelName, list.length, numberOfEntities),
            (e: any) => this.service.notifier && this.service.notifier.openErrorNotification(e)
          ),
          map(res => this.mapPaginatedResponse(res, apiCall)));
      case ResponseTypeEnum.paginated_post:
        return this.service.getAllPaginatedBody(apiCall, this.parentUuId).pipe(
          tap(
            ({list, numberOfEntities}) => this.service.notifier && this.service.notifier.openGetListNotification(this.service.config.modelName, list.length, numberOfEntities),
            (e: any) => this.service.notifier && this.service.notifier.openErrorNotification(e)
          ),
          map(res => this.mapPaginatedResponse(res, apiCall)));
      case ResponseTypeEnum.all:
      default:
        return this.service.getAll(this.parentUuId)
          .pipe(tap(
            response => this.service.notifier && this.service.notifier.openGetListNotification(this.service.config.modelName, (response as any).length),
            (e: any) => this.service.notifier && this.service.notifier.openErrorNotification(e)
          ));
    }
  }

  /**
   * sets up length of received array & returns data list
   * is arrow function to survive this
   * @param pagination: Paginated Response Model
   * @param apiCall: Api call model
   */
  mapPaginatedResponse(pagination: any, apiCall: ApiCallModel): any {
    this.paginator = {
      ...this.paginator,
      length: pagination.totalItems || pagination.numberOfEntities || 0,
      pageIndex: apiCall.paginator.pageIndex,
      pageSize: apiCall.paginator.pageSize
    };
    return pagination.list;
  }

  /**
   * puts API response into data;
   * then calls afterDataInit()
   * @param res - API response
   */
  onSuccessfulDataInit(res: T[]): void {
    this.data = res;
    this.afterDataInit();
  }

  /**
   * DETAILS
   */
  /**
   * navigates to this.route/{element[keyValue]}
   * @param t: list element
   */
  openDetails(t: T): void {
    this.router.navigate([t[this.service.keyValue]], {relativeTo: this.route});
  }

  /** NEW DIALOG */
  /**
   * calls onBeforeOpen(), opens dialog & subscribes on result. Result is send to afterNewClosed()
   * */
  openNew(): void {
    this.onBeforeNewOpen();
    const dialogRef = this.dialog.open(this.returnNewComponentType() || this.returnEditComponentType(), this.dialog_config);
    dialogRef.afterClosed().subscribe(res => this.afterNewClosed(res));
  }

  /**
   * calls from openNew()
   * setups dialog data, panelClasses & width
   * if newDialogWidth is undefined takes editDialogWidth
   * if newDialogPanelClass is undefined takes editDialogPanelClass
   */
  onBeforeNewOpen(): void {
    this.abstract_dialog_config.element = {...this.newElement} as T;
    this.dialog_config.data = this.abstract_dialog_config;
    this.dialog_config.width = this.newDialogWidth || this.editDialogWidth;
    this.dialog_config.panelClass = [this.dialogPanelClass, this.newDialogPanelClass || this.editDialogPanelClass];
  }

  /**
   * if submit true calls onSuccessfulCreateNew()
   * when false onNewCreateNewCancel()
   * @param submit: true when created/false when canceled
   */
  afterNewClosed(submit: boolean): void {
    submit ? this.onSuccessfulCreateNew() : this.onCreateNewCancel();
  }

  /**
   * calls getData()
   */
  onSuccessfulCreateNew(): void {
    this.getData();
  }


  onCreateNewCancel() {
  }

  /** EDIT DIALOG */
  /**
   * calls onBeforeEditOpen()
   * opens dialog
   * subscribe on dialog afterClosed and emits value into afterEditClosed()
   * @param t: element
   */
  openEdit(t: T) {
    this.onBeforeEditOpen(t);
    const dialogRef = this.dialog.open(this.returnEditComponentType(), this.dialog_config);
    dialogRef.afterClosed().subscribe(res => this.afterEditClose(res));
  }

  /**
   * if submit true calls onSuccessfulCreateNew()
   * when false onNewCreateNewCancel()
   * @param submit: true when edited/false when canceled
   */
  afterEditClose(submit: boolean | any): void {
    submit ? this.onSuccessfulEdit(submit) : this.onEditCancel();
  }

  /**
   * if submit true calls onSuccessfulDelete()
   * when false onDeleteCancel()
   * @param submit: true when edited/false when canceled
   * @param t: element to delete
   */
  afterDeleteClose(submit: boolean, t: T | any): void {
    submit ? this.onSuccessfulDelete(submit, t) : this.onDeleteCancel();
  }

  /**
   * calls getData()
   * @param submit: true when edited/false when canceled
   * @param t: element to delete
   */
  onSuccessfulDelete(submit, t): void {
    this.getData();
  }

  /**
   * use for dynamic table @Output(actionRequest)
   * handles dynamic table events: edit, delete, open;
   * if do not need open override this way:
   * @example
   * > if ($event.type === 'open) return;
   * > super.actionHandler($event);
   * @param $event: dynamic table event: {type: string, entity: element}
   */
  actionHandler($event: { type: string, entity: T }): void {
    switch ($event.type) {
      case ('delete'):
        this.openDelete($event.entity);
        break;
      case ('edit'):
        this.openEdit($event.entity);
        break;
      case ('open'):
        this.openDetails($event.entity);
        break;
    }
  }

}
