// import {AbstractListComponent} from './abstract-list.component';
// import {Component} from '@angular/core';
// import {MatDialog} from '@angular/material';
// import {ActivatedRoute, Router} from '@angular/router';
// import {ComponentType} from '@angular/cdk/portal';
// import {AbstractDeleteComponent} from '../../abstract-dialog/abstract-delete/abstract-delete.component';
// import {AbstractEditComponent} from '../../abstract-dialog/abstract-edit/abstract-edit.component';
// import {async, ComponentFixture, TestBed} from '@angular/core/testing';
// import {of} from 'rxjs';
// import {AbstractService} from '../../providers/abstract.service';
// import {ResponseTypeEnum} from '../../config/response-type.enum';
//
// describe('AbstractListComponent', () => {
//   let component: AbstractListComponent<T>;
//   let fixture: ComponentFixture<AbstractListComponent<T>>;
//
//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       declarations: [ListComponent],
//       providers: [
//         {
//           provide: AbstractService, useValue: {
//             get: jasmine.createSpy('get').and.returnValue(of(test_data))
//           }
//         },
//         {provide: MatDialog, useValue: {}},
//         {provide: Router, useValue: {}},
//         {provide: ActivatedRoute, useValue: {}}
//       ]
//     })
//       .compileComponents();
//   }));
//
//   beforeEach(() => {
//     fixture = TestBed.createComponent(ListComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });
//
//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
//
//   it('should have provided data', () => {
//     expect(component.data).toEqual(test_data);
//   });
// });
//
// class T {
//   id: {};
//   uuId: string;
// }
//
// @Component({
//   selector: 'bd-test-list',
//   template: ''
// })
// class ListComponent extends AbstractListComponent<T> {
//   listType = ResponseTypeEnum.all;
//   public constructor(public service: AbstractService<T>,
//                      public dialog: MatDialog,
//                      public router: Router,
//                      public route: ActivatedRoute) {
//     super(service, dialog, router, route);
//   }
//
//   returnDeleteComponentType(): ComponentType<AbstractDeleteComponent<T>> {
//     return undefined;
//   }
//
//   returnEditComponentType(): ComponentType<AbstractEditComponent<T>> {
//     return undefined;
//   }
//
// }
//
// const test_data = [
//   {uuId: '0123', id: {}},
//   {uuId: '1234', id: {}}
// ];
