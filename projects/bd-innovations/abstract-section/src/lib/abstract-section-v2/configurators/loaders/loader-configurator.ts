import {Subject} from 'rxjs';

export interface LoaderConfigurator<D> {
  onDataLoaded$: Subject<any>

  initLoader(): void;

  loadData(requestParams?: any): void

  destroyLoader(): void;
}
