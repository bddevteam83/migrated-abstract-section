import {CrudService} from '../../../providers/crud.service';
import {mixinHasSubs} from '../../../mixins/has-subs.mixin';
import {LoaderConfigurator} from '../loader-configurator';
import {Observable, Subject} from 'rxjs';
import {switchMap} from 'rxjs/operators';

export interface CanHaveOneLoader<D> {
  primaryKeyFactory: () => Observable<number | string>
  service: CrudService<D>,
}

export class OneLoaderConfigurator<D> extends mixinHasSubs(class {
}) implements LoaderConfigurator<D> {

  element: D;

  onDataLoaded$: Subject<D> = new Subject<D>();

  constructor(protected owner: CanHaveOneLoader<D>) {
    super()
  }

  initLoader(): void {
    this.loadData()
  }

  loadData(): void {
    this.subs$.push(
      this.owner.primaryKeyFactory().pipe(
        switchMap(primaryKey => this.owner.service.getOne(primaryKey))
      ).subscribe(next => {
        this.element = next;
        this.onDataLoaded$.next(this.element);
      })
    )
  }

  destroyLoader(): void {
    this.clearSubscriptions()
  }

}
