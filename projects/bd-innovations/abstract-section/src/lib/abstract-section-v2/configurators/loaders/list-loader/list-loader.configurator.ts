import {CrudService} from '../../../providers/crud.service';
import {mixinHasSubs} from '../../../mixins/has-subs.mixin';
import {LoaderConfigurator} from '../loader-configurator';
import {Subject} from 'rxjs';


export interface CanHaveListLoader<D> {
  service: CrudService<D>;
  parentPrimaryKey?: number | string;
}

export class ListLoaderConfigurator<D> extends mixinHasSubs(class {
}) implements LoaderConfigurator<D> {

  data: D[];

  onDataLoaded$: Subject<D[]> = new Subject<D[]>();

  parentPrimaryKey: string | number;

  constructor(protected owner: CanHaveListLoader<D>) {
    super();
  }

  initLoader(): void {
    this.loadData(this.owner.parentPrimaryKey);
  }

  loadData(parentPrimaryKey?: string | number): void {
    this.parentPrimaryKey = parentPrimaryKey !== undefined ? parentPrimaryKey : this.parentPrimaryKey;

    this.subs$.push(
      this.owner.service.getList(this.parentPrimaryKey)
        .subscribe(next => {
          this.data = next;
          this.onDataLoaded$.next(this.data);
        })
    );
  }

  destroyLoader() {
    this.clearSubscriptions();
  }

}
