import {mixinHasSubs} from '../../../mixins/has-subs.mixin';
import {PaginatedResponseConfig} from '../../../configs/paginated-response.config';
import {PaginatedRequestConfig} from '../../../configs/paginated-request.config';
import {LoaderConfigurator} from '../loader-configurator';
import {Subject} from 'rxjs';
import {CrudService} from '../../../providers/crud.service';


export interface CanHavePaginatedListLoader<D> {
  service: CrudService<D>;
  requestParams?: PaginatedRequestConfig;
}

export class PaginatedListLoaderConfigurator<D> extends mixinHasSubs(class {
}) implements LoaderConfigurator<D> {

  data: PaginatedResponseConfig<D>;

  onDataLoaded$: Subject<PaginatedResponseConfig<D>> = new Subject<PaginatedResponseConfig<D>>();

  requestParams: PaginatedRequestConfig = {
    pagination: {
      pageSize: 25,
      pageIndex: 0
    }
  };

  constructor(protected owner: CanHavePaginatedListLoader<D>) {
    super();
  }

  initLoader(): void {
    this.requestParams = {
      ...this.requestParams,
      ...this.owner.requestParams
    };

    this.loadData(this.requestParams);
  }

  loadData(requestParams?: PaginatedRequestConfig): void {
    this.requestParams = {
      ...this.requestParams,
      ...requestParams
    };

    this.subs$.push(
      this.owner.service.getPaginatedList(this.requestParams)
        .subscribe(next => {
          this.data = next;
          this.onDataLoaded$.next(this.data);
        })
    );
  }

  destroyLoader(): void {
    this.clearSubscriptions();
  }

}
