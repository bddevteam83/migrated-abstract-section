import {FormGroup} from '@angular/forms';
import {LogicService} from '../../../providers/logic.service';
import {LoaderConfigurator} from '../loader-configurator';
import {Subject} from 'rxjs';


export interface CanHaveFormLoader<D> {
  logic: LogicService<D>,
  data: {
    element?: D
  }
}

export class FormLoaderConfigurator<D> implements LoaderConfigurator<D> {

  form: FormGroup;

  onDataLoaded$: Subject<D> = new Subject<D>();

  constructor(protected owner: CanHaveFormLoader<D>) {
  }

  initLoader() {
    this.form = this.owner.logic.form(this.owner.data.element);
    this.onDataLoaded$.next(this.owner.data.element)
  };

  loadData(): void {
  }

  destroyLoader(): void {
  }

}
