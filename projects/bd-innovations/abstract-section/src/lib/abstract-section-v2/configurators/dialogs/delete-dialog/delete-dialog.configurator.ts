import {MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material/dialog';
import {ComponentType} from '@angular/cdk/portal';
import {Subject} from 'rxjs';
import {mixinHasSubs} from '../../../mixins/has-subs.mixin';
import {DialogConfigurator} from '../dialog.configurator';
import {DialogComponentFasade} from '../../../fasades/dialog-component.fasade';
import {LogicService} from '../../../providers/logic.service';
import {CrudService} from '../../../providers/crud.service';


export interface CanHaveDeleteDialog<C extends DialogComponentFasade, D> {
  service: CrudService<D>;
  logic: LogicService<D>;
  dialog: MatDialog;
  deleteComponentType: ComponentType<C>
}

export class DeleteDialogConfigurator<C extends DialogComponentFasade<D>, D> extends mixinHasSubs(class {
}) implements DialogConfigurator<C, D> {

  onSuccess$: Subject<void> = new Subject<void>();
  onCancel$: Subject<void> = new Subject<void>();

  protected dialogRef: MatDialogRef<C>;

  constructor(protected owner: CanHaveDeleteDialog<C, D>) {
    super()
  }

  openDialog(instance: D) {
    const dialogConfig: MatDialogConfig = {
      data: {
        element: instance
      }
    };

    this.dialogRef = this.owner.dialog.open(this.owner.deleteComponentType, dialogConfig);
    const dialogComponent: DialogComponentFasade = this.dialogRef.componentInstance;

    this.subs$.push(
      dialogComponent.submit$
        .subscribe(() => this.onDialogSubmit(instance)),
      dialogComponent.cancel$
        .subscribe(() => this.onDialogCancel())
    )
  }

  protected onDialogSubmit(res: D): void {
    this.subs$.push(
      this.owner.service.delete(res[this.owner.logic.primaryKey])
        .subscribe(() => {
          this.dialogRef.close();
          this.onSuccess$.next();
        })
    )
  }

  protected onDialogCancel(): void {
    this.dialogRef.close();
  }

}
