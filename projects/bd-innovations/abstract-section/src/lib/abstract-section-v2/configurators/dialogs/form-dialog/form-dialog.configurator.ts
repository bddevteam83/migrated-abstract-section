import {Subject} from 'rxjs';
import {MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material/dialog';
import {ComponentType} from '@angular/cdk/portal';
import {mixinHasSubs} from '../../../mixins/has-subs.mixin';
import {DialogConfigurator} from '../dialog.configurator';
import {LogicService} from '../../../providers/logic.service';
import {CrudService} from '../../../providers/crud.service';
import {DialogComponentFasade} from '../../../fasades/dialog-component.fasade';


export interface CanHaveFormDialog<C extends DialogComponentFasade, D> {
  service: CrudService<D>;
  logic: LogicService<D>;
  dialog: MatDialog;
  dialogConfig?: {
    reloadData?: boolean;
    additionalDialogData?: { [key: string]: any }
  }
  formComponentType: ComponentType<C>
}

export class FormDialogConfigurator<C extends DialogComponentFasade, D> extends mixinHasSubs(class {
}) implements DialogConfigurator<C, D> {

  onSuccess$: Subject<void> = new Subject<void>();
  onCancel$: Subject<void> = new Subject<void>();

  protected dialogRef: MatDialogRef<C>;

  constructor(protected owner: CanHaveFormDialog<C, D>) {
    super()
  }

  async openDialog(instance?: D) {
    if (
      this.owner.dialogConfig && this.owner.dialogConfig.reloadData &&
      instance && instance[this.owner.logic.primaryKey]
    ) {
      instance = await this.owner.service.getOne(instance[this.owner.logic.primaryKey]).toPromise();
    }

    const dialogConfig: MatDialogConfig = {
      data: {
        element: instance,
        ...(this.owner.dialogConfig && this.owner.dialogConfig.additionalDialogData)
      }
    };

    this.dialogRef = this.owner.dialog.open(this.owner.formComponentType, dialogConfig);
    const dialogComponent: DialogComponentFasade = this.dialogRef.componentInstance;

    this.subs$.push(
      dialogComponent.submit$
        .subscribe(res => this.onDialogSubmit(res)),
      dialogComponent.cancel$
        .subscribe(() => this.onDialogCancel())
    )
  }

  protected onDialogSubmit(res: D): void {
    this.subs$.push(
      (res[this.owner.logic.primaryKey] ?
        this.owner.service.put(res) :
        this.owner.service.post(res))
        .subscribe(() => {
          this.dialogRef.close();
          this.onSuccess$.next();
        })
    )
  }

  protected onDialogCancel(): void {
    this.dialogRef.close();
  }

}
