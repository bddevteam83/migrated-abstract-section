import {Subject} from 'rxjs';
import {DialogComponentFasade} from '../../fasades/dialog-component.fasade';

export interface DialogConfigurator<C extends DialogComponentFasade, D = any> {
  onSuccess$: Subject<void>;
  onCancel$: Subject<void>;

  openDialog(instance?: D): void;
}
