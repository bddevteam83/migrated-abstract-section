import {mixinHasSubs} from './has-subs.mixin';
import {of} from 'rxjs';

class TestClass extends mixinHasSubs(class {
}) {
  constructor() {
    super();
  }
}

describe('MixinHasSubs', () => {
  let klass: TestClass;

  beforeEach(() => {
    klass = new TestClass();
  });

  it('should be created', () => {
    expect(klass).toBeTruthy();
  });

  it('should have subs$ property as an array of subscriptions', function () {
    expect(klass.subs$).toBeTruthy();
    expect(klass.subs$ instanceof Array).toBeTruthy();
    //  TODO test that subs$ is array exactly of subscriptions
    expect(klass.subs$).toEqual([])
  });

  describe('#clearSubscriptions()', () => {
    it('should unsubscribe from all subscriptions in subs$ array', function () {
      klass.subs$.concat(
        Array.apply(null, {length: 3})
          .map(elem => of(elem).subscribe(() => {
          }))
      );

      klass.clearSubscriptions();

      expect(klass.subs$.every(sub => sub.closed)).toBeTruthy()
    });
  })
});
