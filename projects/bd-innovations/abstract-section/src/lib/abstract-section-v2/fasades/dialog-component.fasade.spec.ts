import {TestBed} from '@angular/core/testing';
import {Component, EventEmitter} from '@angular/core';
import {DialogComponentFasade} from './dialog-component.fasade';
import Spy = jasmine.Spy;

@Component({
  selector: 'bd-test',
  template: '<p>test works!</p>',
  styles: []
})
class TestComponent extends DialogComponentFasade {
  constructor() {
    super();
  }
}

describe('DialogComponentFasade', () => {
  let component: TestComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TestComponent]
    }).compileComponents();

    component = TestBed.createComponent(TestComponent).componentInstance
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should extend mixinHasSubs', function () {
    // TODO test extending things via mixin
    //  expect(component instanceof HaveSubsCtor).toBeTruthy()
  });

  it('should have cancel$ and submit$ event emitters', function () {
    expect(component.cancel$).toBeTruthy();
    expect(component.cancel$ instanceof EventEmitter).toBeTruthy();

    expect(component.submit$).toBeTruthy();
    expect(component.submit$ instanceof EventEmitter).toBeTruthy()
  });

  describe('#ngOnInit()', () => {
    it('should call loaderConfigurator #initLoader() if loaderConfigurator is presented', function () {
      component.loaderConfigurator = {
        initLoader: () => {
        }
      } as any;

      const loaderConfiguratorInitLoaderSpy: Spy = spyOn(component.loaderConfigurator, 'initLoader');

      component.ngOnInit();

      expect(loaderConfiguratorInitLoaderSpy.calls.count()).toBe(1)
    });
  });

  describe('#ngOnDestroy()', () => {
    it('should call loaderConfigurator #destroyLoader() if loaderConfigurator is presented', function () {
      component.loaderConfigurator = {
        destroyLoader: () => {
        }
      } as any;

      const loaderConfiguratorDestroyLoaderSpy: Spy = spyOn(component.loaderConfigurator, 'destroyLoader');

      component.ngOnDestroy();

      expect(loaderConfiguratorDestroyLoaderSpy.calls.count()).toBe(1)
    });
  });
});
