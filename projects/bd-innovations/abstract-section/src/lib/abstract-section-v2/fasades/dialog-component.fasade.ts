import {EventEmitter, OnDestroy, OnInit} from '@angular/core';
import {LoaderConfigurator} from '../configurators/loaders/loader-configurator';

export abstract class DialogComponentFasade<D = any> implements OnInit, OnDestroy {

  loaderConfigurator: LoaderConfigurator<D>;

  cancel$: EventEmitter<void> = new EventEmitter<void>();
  submit$: EventEmitter<any> = new EventEmitter<any>();

  protected constructor() {
  }

  ngOnInit(): void {
    this.loaderConfigurator && this.loaderConfigurator.initLoader()
  }

  ngOnDestroy(): void {
    this.loaderConfigurator && this.loaderConfigurator.destroyLoader()
  }

}
