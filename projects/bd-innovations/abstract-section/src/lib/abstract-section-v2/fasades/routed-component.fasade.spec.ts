import {TestBed} from '@angular/core/testing';
import {Component} from '@angular/core';
import {RoutedComponentFasade} from './routed-component.fasade';
import Spy = jasmine.Spy;

@Component({
  selector: 'bd-test',
  template: '<p>test works!</p>',
  styles: []
})
class TestComponent extends RoutedComponentFasade<any> {
  constructor() {
    super();
  }
}

describe('RoutedComponentFasade', () => {
  let component: TestComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TestComponent]
    }).compileComponents();

    component = TestBed.createComponent(TestComponent).componentInstance
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should extend mixinHasSubs', function () {
    // TODO test extending things via mixin
    //  expect(component instanceof HaveSubsCtor).toBeTruthy()
  });

  describe('#ngOnInit()', () => {
    it('should call three init methods if appropriate configurators are presented', function () {
      component.loaderConfigurator = {
        initLoader: () => {
        }
      } as any;
      component.formConfigurator = {
        initDataReloadOnSuccessNewOrEdit: () => {
        }
      } as any;
      component.deleteConfigurator = {
        initDataReloadOnSuccessDelete: () => {
        }
      } as any;

      const loaderConfiguratorInitLoaderSpy: Spy = spyOn(component.loaderConfigurator, 'initLoader');
      const initDataReloadOnSuccessNewOrEditSpy: Spy = spyOn(component, 'initDataReloadOnSuccessNewOrEdit');
      const initDataReloadOnSuccessDeleteSpy: Spy = spyOn(component, 'initDataReloadOnSuccessDelete');

      component.ngOnInit();

      expect(loaderConfiguratorInitLoaderSpy.calls.count()).toBe(1);
      expect(initDataReloadOnSuccessNewOrEditSpy.calls.count()).toBe(1);
      expect(initDataReloadOnSuccessDeleteSpy.calls.count()).toBe(1)
    });

    it('should not call neither of three init methods if appropriate configurators aren\'t presented', function () {
      //  TODO how to spy on object methods if object is undefined?
    });
  });

  describe('#ngOnDestroy()', () => {
    it('should call three destroy methods if appropriate configurators are presented and self #clearSubscriptions()', function () {
      component.loaderConfigurator = {
        destroyLoader: () => {
        }
      } as any;
      component.formConfigurator = {
        clearSubscriptions: () => {
        }
      } as any;
      component.deleteConfigurator = {
        clearSubscriptions: () => {
        }
      } as any;

      const loaderConfiguratorDestroyLoaderSpy: Spy = spyOn(component.loaderConfigurator, 'destroyLoader');
      const formConfiguratorClearSubscriptionsSpy: Spy = spyOn(component.formConfigurator, 'clearSubscriptions');
      const deleteConfiguratorClearSubscriptionsSpy: Spy = spyOn(component.deleteConfigurator, 'clearSubscriptions');
      const clearSubscriptionsSpy: Spy = spyOn(component, 'clearSubscriptions');

      component.ngOnDestroy();

      expect(loaderConfiguratorDestroyLoaderSpy.calls.count()).toBe(1);
      expect(formConfiguratorClearSubscriptionsSpy.calls.count()).toBe(1);
      expect(deleteConfiguratorClearSubscriptionsSpy.calls.count()).toBe(1);
      expect(clearSubscriptionsSpy.calls.count()).toBe(1);
    });

    it('should not call three destroy methods if appropriate configurators aren\'t presented but still call self #clearSubscriptions()', function () {
      //  TODO how to spy on object methods if object is undefined?
    });
  });

  describe('#initDataReloadOnSuccessNewOrEdit()', () => {
    it('should subscribe for formConfigurator onSuccess$ to recall loaderConfigurator #initLoader() and push subscription to subs$ array', function () {
      // TODO implement it
    });
  });

  describe('#initDataReloadOnSuccessDelete()', () => {
    it('should subscribe for formConfigurator onSuccess$ to recall loaderConfigurator #initLoader() and push subscription to subs$ array', function () {
      // TODO implement it
    });
  });
});
