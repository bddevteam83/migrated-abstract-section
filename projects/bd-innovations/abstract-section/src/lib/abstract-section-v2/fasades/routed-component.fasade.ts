import {OnDestroy, OnInit} from '@angular/core';
import {DeleteDialogConfigurator} from '../configurators/dialogs/delete-dialog/delete-dialog.configurator';
import {FormDialogConfigurator} from '../configurators/dialogs/form-dialog/form-dialog.configurator';
import {mixinHasSubs} from '../mixins/has-subs.mixin';
import {LoaderConfigurator} from '../configurators/loaders/loader-configurator';
import {DialogComponentFasade} from './dialog-component.fasade';

export abstract class RoutedComponentFasade<D> extends mixinHasSubs(class {
}) implements OnInit, OnDestroy {

  loaderConfigurator: LoaderConfigurator<D>;
  formConfigurator: FormDialogConfigurator<DialogComponentFasade, D>;
  deleteConfigurator: DeleteDialogConfigurator<DialogComponentFasade, D>;

  protected constructor() {
    super()
  }

  ngOnInit(): void {
    this.loaderConfigurator && this.loaderConfigurator.initLoader();
    this.formConfigurator && this.initDataReloadOnSuccessNewOrEdit();
    this.deleteConfigurator && this.initDataReloadOnSuccessDelete();
  }

  ngOnDestroy(): void {
    this.clearSubscriptions();
    this.loaderConfigurator && this.loaderConfigurator.destroyLoader();
    this.formConfigurator && this.formConfigurator.clearSubscriptions();
    this.deleteConfigurator && this.deleteConfigurator.clearSubscriptions();
  }

  initDataReloadOnSuccessNewOrEdit() {
    this.subs$.push(
      this.formConfigurator.onSuccess$
        .subscribe(() => this.loaderConfigurator.loadData())
    )
  }

  initDataReloadOnSuccessDelete() {
    this.subs$.push(
      this.deleteConfigurator.onSuccess$
        .subscribe(() => this.loaderConfigurator.loadData())
    );
  }

}
