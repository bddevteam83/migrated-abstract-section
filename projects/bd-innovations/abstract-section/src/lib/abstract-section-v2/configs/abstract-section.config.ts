import {MatDialogConfig} from '@angular/material/dialog';
import {InjectionToken} from '@angular/core';

export const ABSTRACT_SECTION_CONFIG: InjectionToken<AbstractSectionConfig> = new InjectionToken<AbstractSectionConfig>('ABSTRACT_SECTION_CONFIG');

export interface AbstractSectionConfig {
  defaultDialogConfig: MatDialogConfig<any>
}
