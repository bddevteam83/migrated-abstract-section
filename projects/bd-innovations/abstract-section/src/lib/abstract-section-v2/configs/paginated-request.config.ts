export interface PaginatedRequestConfig {
  pagination: PaginationRequestModel;
  parentPrimaryKey?: number | string;
  sort?: SortRequestModel;
  filters?: { [key: string]: any }
  indexSearch?: string;
}

export interface PaginationRequestModel {
  pageIndex: number;
  pageSize: number;
}

export interface SortRequestModel {
  active: string;
  direction: UpperCaseSortDirection;
}

export type UpperCaseSortDirection = 'ASC' | 'DESC' | null;


