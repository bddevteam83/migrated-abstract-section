export interface PaginatedResponseConfig<T> {
  list: T[];
  totalItems: number;
}
