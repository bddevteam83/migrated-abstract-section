export interface CrudApiConfig {
  apiPrefix: string;
  apiPath: {
    getOne: string;
    getMany: string;
    post: string;
    put: string;
    delete: string;
  };
}
