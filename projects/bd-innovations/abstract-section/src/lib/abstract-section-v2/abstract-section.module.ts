import {ModuleWithProviders, NgModule} from '@angular/core';
import {ABSTRACT_SECTION_CONFIG, AbstractSectionConfig} from './configs/abstract-section.config';

@NgModule()
export class AbstractSectionModule {
  static forRoot(config: AbstractSectionConfig): ModuleWithProviders<AbstractSectionModule> {
    return {
      ngModule: AbstractSectionModule,
      providers: [
        {
          provide: ABSTRACT_SECTION_CONFIG,
          useValue: config.defaultDialogConfig
        }
      ]
    }
  }
}
