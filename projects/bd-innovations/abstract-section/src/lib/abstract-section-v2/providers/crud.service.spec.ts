import {TestBed} from '@angular/core/testing';
import {CrudService} from './crud.service';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {PaginatedRequestConfig} from '../configs/paginated-request.config';

@Injectable()
class TestService extends CrudService<any> {
  constructor(protected http: HttpClient) {
    super(http, {
      apiPrefix: 'api-prefix',
      apiPath: {
        getOne: 'get-one-path',
        getMany: 'get-many-path',
        post: 'post-path',
        put: 'put-path',
        delete: 'delete',
      }
    });
  }
}

describe('CrudService', () => {
  let service: TestService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TestService]
    });
    service = TestBed.get(TestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('(tests with HttpClient)', () => {
    let httpTestingController: HttpTestingController;

    beforeEach(() => {
      httpTestingController = TestBed.get(HttpTestingController)
    });

    afterEach(() => {
      httpTestingController.verify()
    });

    describe('#getOne(primaryKey)', () => {
      it('should send HTTP GET request according to it\'s config and passed primaryKey and return it\'s response', (done: DoneFn) => {
        const primaryKey = 'primary-key';
        const mockRes = 'mocked-result';

        service.getOne(primaryKey)
          .subscribe(next => {
            expect(next).toEqual(mockRes);
            done()
          });

        const req = httpTestingController.expectOne({
          url: `api-prefix/get-one-path/${primaryKey}`,
          method: 'GET'
        });

        req.flush(mockRes)
      });

      it('should throw en error if passed primaryKey is falsy or neither number or string', function () {
        const falsyPrimaryKey: any = null;
        const objectPrimaryKey: any = {};
        const errorMessage = 'CrudService #getOne() primaryKey is falsy or neither number or string';

        expect(() => service.getOne(falsyPrimaryKey)).toThrowError(errorMessage);
        expect(() => service.getOne(objectPrimaryKey)).toThrowError(errorMessage);

        httpTestingController.expectNone('api-prefix/get-one-path/null');
        httpTestingController.expectNone('api-prefix/get-one-path/[object Object]')
      });
    });

    describe('#getList(parentPrimaryKey)', () => {
      it('should send HTTP GET request according to it\'s config and return it\'s response', function (done: DoneFn) {
        const mockRes = 'mocked-result';

        service.getList()
          .subscribe(next => {
            expect(next).toEqual(mockRes);
            done();
          });

        const req = httpTestingController.expectOne({
          url: 'api-prefix/get-many-path',
          method: 'GET'
        });

        req.flush(mockRes)
      });

      it('should send HTTP GET request according to it\'s config and passed parentPrimaryKey and return it\'s response', function (done: DoneFn) {
        const parentPrimaryKey = 'parent-primary-key';
        const mockRes = 'mocked-result';

        service.getList(parentPrimaryKey)
          .subscribe(next => {
            expect(next).toEqual(mockRes);
            done();
          });

        const req = httpTestingController.expectOne({
          url: `api-prefix/get-many-path/${parentPrimaryKey}`,
          method: 'GET'
        });

        req.flush(mockRes);
      });

      it('should throw en error if passed parentPrimaryKey neither number or string', function () {
        const objectPrimaryKey: any = {};
        const errorMessage = 'CrudService #getList() parentPrimaryKey was passed but it is neither number or string';

        expect(() => service.getList(objectPrimaryKey)).toThrowError(errorMessage);

        httpTestingController.expectNone(`api-prefix/get-one-path/${objectPrimaryKey}`)
      });
    });

    describe('#getPaginatedList(requestParams)', () => {
      it('should send HTTP GET request according to it\' config and passed requestParams and return it\'s response (only pagination)', function (done: DoneFn) {
        const requestParams: PaginatedRequestConfig = {
          pagination: {
            pageIndex: 0,
            pageSize: 25
          }
        };
        const mockRes: any = 'mocked-result';

        service.getPaginatedList(requestParams)
          .subscribe(next => {
            expect(next).toEqual(mockRes);
            done();
          });

        const req = httpTestingController.expectOne({
          url: `api-prefix/get-many-path/${requestParams.pagination.pageIndex}/${requestParams.pagination.pageSize}`,
          method: 'GET'
        });

        req.flush(mockRes);
      });

      it('should send HTTP GET request according to it\' config and passed requestParams and return it\'s response (pagination with parentPrimaryKey)', function (done: DoneFn) {
        const requestParams: PaginatedRequestConfig = {
          pagination: {
            pageIndex: 0,
            pageSize: 25
          },
          parentPrimaryKey: 'parent-primary-key'
        };
        const mockRes: any = 'mocked-result';

        service.getPaginatedList(requestParams)
          .subscribe(next => {
            expect(next).toEqual(mockRes);
            done();
          });

        const req = httpTestingController.expectOne({
          url: `api-prefix/get-many-path/${requestParams.parentPrimaryKey}/${requestParams.pagination.pageIndex}/${requestParams.pagination.pageSize}`,
          method: 'GET'
        });

        req.flush(mockRes);
      });

      it('should send HTTP GET request according to it\' config and passed requestParams and return it\'s response (pagination with parentPrimaryKey and sort)', function (done: DoneFn) {
        const requestParams: PaginatedRequestConfig = {
          pagination: {
            pageIndex: 0,
            pageSize: 25
          },
          parentPrimaryKey: 'parent-primary-key',
          sort: {
            active: 'activeField',
            direction: 'ASC'
          }
        };
        const mockRes: any = 'mocked-result';

        service.getPaginatedList(requestParams)
          .subscribe(next => {
            expect(next).toEqual(mockRes);
            done();
          });

        const req = httpTestingController.expectOne(req => (
          req.url === `api-prefix/get-many-path/${requestParams.parentPrimaryKey}/${requestParams.pagination.pageIndex}/${requestParams.pagination.pageSize}`
          && req.method === 'GET'
        ));

        expect(req.request.params.has('sort')).toBeTruthy();
        expect(req.request.params.get('sort')).toEqual(`${requestParams.sort.active},${requestParams.sort.direction}`);

        req.flush(mockRes);
      });

      it('should send HTTP GET request according to it\' config and passed requestParams and return it\'s response (pagination with parentPrimaryKey, sort and filters)', function (done: DoneFn) {
        const requestParams: PaginatedRequestConfig = {
          pagination: {
            pageIndex: 0,
            pageSize: 25
          },
          parentPrimaryKey: 'parent-primary-key',
          sort: {
            active: 'activeField',
            direction: 'ASC'
          },
          filters: {
            someDate: new Date(),
            someField: 'some-field'
          }
        };
        const mockRes: any = 'mocked-result';

        service.getPaginatedList(requestParams)
          .subscribe(next => {
            expect(next).toEqual(mockRes);
            done();
          });

        const req = httpTestingController.expectOne(req => (
          req.url === `api-prefix/get-many-path/${requestParams.parentPrimaryKey}/${requestParams.pagination.pageIndex}/${requestParams.pagination.pageSize}`
          && req.method === 'GET'
        ));

        expect(req.request.params.has('sort')).toBeTruthy();
        expect(req.request.params.get('sort')).toEqual(`${requestParams.sort.active},${requestParams.sort.direction}`);
        expect(req.request.params.has('someDate')).toBeTruthy();
        expect(req.request.params.get('someDate')).toEqual((requestParams.filters.someDate as Date).toISOString(), 'should map dates to ISO strings passed throw query params');
        expect(req.request.params.has('someField')).toBeTruthy();
        expect(req.request.params.get('someField')).toEqual(requestParams.filters.someField);

        req.flush(mockRes);
      });

      it('should send HTTP GET request according to it\' config and passed requestParams and return it\'s response (pagination with parentPrimaryKey, sort, filters and indexSearch)', function (done: DoneFn) {
        const requestParams: PaginatedRequestConfig = {
          pagination: {
            pageIndex: 0,
            pageSize: 25
          },
          parentPrimaryKey: 'parent-primary-key',
          sort: {
            active: 'activeField',
            direction: 'ASC'
          },
          filters: {
            someDate: new Date(),
            someField: 'some-field'
          },
          indexSearch: 'index-search'
        };
        const mockRes: any = 'mocked-result';

        service.getPaginatedList(requestParams)
          .subscribe(next => {
            expect(next).toEqual(mockRes);
            done();
          });

        const req = httpTestingController.expectOne(req => (
          req.url === `api-prefix/get-many-path/${requestParams.parentPrimaryKey}/${requestParams.pagination.pageIndex}/${requestParams.pagination.pageSize}`
          && req.method === 'GET'
        ));

        expect(req.request.params.has('sort')).toBeTruthy();
        expect(req.request.params.get('sort')).toEqual(`${requestParams.sort.active},${requestParams.sort.direction}`);
        expect(req.request.params.has('someDate')).toBeTruthy();
        expect(req.request.params.get('someDate')).toEqual((requestParams.filters.someDate as Date).toISOString(), 'should map dates to ISO strings passed throw query params');
        expect(req.request.params.has('someField')).toBeTruthy();
        expect(req.request.params.get('someField')).toEqual(requestParams.filters.someField);
        expect(req.request.params.has('search')).toBeTruthy();
        expect(req.request.params.get('search')).toEqual(requestParams.indexSearch);

        req.flush(mockRes);
      });

      it('should send HTTP GET request according to it\' config and passed requestParams and return it\'s response (but not send sort if inner properties are falsy)', function (done: DoneFn) {
        const requestParams: PaginatedRequestConfig = {
          pagination: {
            pageIndex: 0,
            pageSize: 25
          },
          sort: {
            active: null,
            direction: null
          }
        };
        const mockRes: any = 'mocked-result';

        service.getPaginatedList(requestParams)
          .subscribe(next => {
            expect(next).toEqual(mockRes);
            done();
          });

        const req = httpTestingController.expectOne(req => (
          req.url === `api-prefix/get-many-path/${requestParams.pagination.pageIndex}/${requestParams.pagination.pageSize}`
          && req.method === 'GET'
        ));

        expect(req.request.params.has('sort')).toBeFalsy();

        req.flush(mockRes);
      });
    });

    describe('#post(body)', () => {
      it('should send HTTP POST request according to it\'s config with provided body and return it\'s response', function (done: DoneFn) {
        const body = 'body';
        const mockRes = 'mocked-result';

        service.post(body)
          .subscribe(next => {
            expect(next).toEqual(mockRes);
            done()
          });

        const req = httpTestingController.expectOne({
          url: 'api-prefix/post-path',
          method: 'POST'
        });

        expect(req.request.body).toEqual(body);

        req.flush(mockRes)
      });
    });

    describe('#put(body)', () => {
      it('should send HTTP PUT request according to it\'s config with provided body and return it\'s response', function (done: DoneFn) {
        const body = 'body';
        const mockRes = 'mocked-result';

        service.put(body)
          .subscribe(next => {
            expect(next).toEqual(mockRes);
            done()
          });

        const req = httpTestingController.expectOne({
          url: 'api-prefix/put-path',
          method: 'PUT'
        });

        expect(req.request.body).toEqual(body);

        req.flush(mockRes)
      });
    });

    describe('#delete(primaryKey)', () => {
      it('should send HTTP DELETE request according to it\'s config and passed primaryKey and return it\'s response', function (done: DoneFn) {
        const primaryKey = 'primary-key';
        const mockRes = 'mocked-result';

        service.delete(primaryKey)
          .subscribe(next => {
            expect(next).toEqual(mockRes);
            done()
          });

        const req = httpTestingController.expectOne({
          url: `api-prefix/post-path/${primaryKey}`,
          method: 'DELETE'
        });

        req.flush(mockRes)
      });

      it('should throw en error if passed primaryKey is falsy or neither number or string', function () {
        const falsyPrimaryKey: any = null;
        const objectPrimaryKey: any = {};
        const errorMessage = 'CrudService #delete() primaryKey is falsy or neither number or string';

        expect(() => service.delete(falsyPrimaryKey)).toThrowError(errorMessage);
        expect(() => service.delete(objectPrimaryKey)).toThrowError(errorMessage);

        httpTestingController.expectNone('api-prefix/get-one-path/null');
        httpTestingController.expectNone('api-prefix/get-one-path/[object Object]')
      });
    });
  })
});
