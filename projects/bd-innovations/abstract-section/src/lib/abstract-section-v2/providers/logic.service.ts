import {FormGroup} from '@angular/forms';

export interface LogicService<D> {
  primaryKey: string;

  form(instance?: D): FormGroup
}
