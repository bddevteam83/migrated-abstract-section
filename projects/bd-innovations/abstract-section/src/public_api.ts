/*
 * Public API Surface of bd-abstract-section
 */

// ABSTRACT SECTION V1 (DEPRECATED)
export * from './lib/abstract-section/abstract-component/abstract.component';
export * from './lib/abstract-section/abstract-component/abstract-details/abstract-details.component';
export * from './lib/abstract-section/abstract-component/abstract-list/abstract-list.component';

export * from './lib/abstract-section/abstract-dialog/abstract-dialog.component';
export * from './lib/abstract-section/abstract-dialog/abstract-edit/abstract-edit.component';
export * from './lib/abstract-section/abstract-dialog/abstract-delete/abstract-delete.component';

export * from './lib/abstract-section/providers/abstract.service';
export * from './lib/abstract-section/providers/abstract-notifier.service';

export * from './lib/abstract-section/config/abstract-dialog.config';
export * from './lib/abstract-section/config/abstract-service.config';
export * from './lib/abstract-section/config/api-call.model';
export * from './lib/abstract-section/config/response-type.enum';
export * from './lib/abstract-section/config/paginated-response.model';

// ABSTRACT SECTION V2
export * from './lib/abstract-section-v2/abstract-section.module';

export * from './lib/abstract-section-v2/fasades/dialog-component.fasade';
export * from './lib/abstract-section-v2/fasades/routed-component.fasade';

export * from './lib/abstract-section-v2/configs/abstract-section.config';
export * from './lib/abstract-section-v2/configs/crud-api.config';
export * from './lib/abstract-section-v2/configs/paginated-request.config';
export * from './lib/abstract-section-v2/configs/paginated-response.config';

export * from './lib/abstract-section-v2/configurators/dialogs/dialog.configurator';
export * from './lib/abstract-section-v2/configurators/dialogs/delete-dialog/delete-dialog.configurator';
export * from './lib/abstract-section-v2/configurators/dialogs/form-dialog/form-dialog.configurator';

export * from './lib/abstract-section-v2/configurators/loaders/loader-configurator';
export * from './lib/abstract-section-v2/configurators/loaders/form-loader/form-loader.configurator';
export * from './lib/abstract-section-v2/configurators/loaders/list-loader/list-loader.configurator';
export * from './lib/abstract-section-v2/configurators/loaders/one-loader/one-loader.configurator';
export *
  from './lib/abstract-section-v2/configurators/loaders/paginated-list-loader/pagianted-list-loader.configurator';

export * from './lib/abstract-section-v2/providers/crud.service';
export * from './lib/abstract-section-v2/providers/logic.service';
